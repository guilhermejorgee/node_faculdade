const express = require('express');
const bodyParser = require('body-parser');
const app = express();
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
const port = 8000;

class Header {
    constructor(header) {
      this.access = header['access'];
    }
}

class Query {
    constructor(query) {
      this.nome = query.nome;
    }
}

class Pessoa {
    constructor(cliente) {
      this.id = cliente.id
      this.nome = cliente.nome
      this.idade = cliente.idade;
    }
}

app.listen(port,()=>{
    console.log("Projeto executando na porta: " + port);
});

app.get('/clientes',(req,res)=>{
    const query = new Query(req.query);
    res.json({nome: `${query.nome}`});
});

app.post('/clientes', (req, res)=>{
    const headers = new Header(req.headers);   
    if(!headers.access == "senha" || headers.access == undefined){
        res.json({error: "Access inválido"})
    }
    const body = new Pessoa(req.body);
    res.json({id: Number(`${body.id}`), nome: `${body.nome}`, idade: Number(`${body.idade}`)});
});

app.get('/funcionarios',(req,res)=>{
    const query = new Query(req.query);
    res.json({nome: `${query.nome}`});
});

app.delete('/funcionarios/:id', (req, res)=>{
    const headers = new Header(req.headers);
    if(!headers.access == "senha" || headers.access == undefined){
        res.json({error: "Access inválido"})
    }
    let param = req.params.id;
    res.json({id: Number(`${param}`)});
});

app.put('/funcionarios', (req, res)=>{
    const headers = new Header(req.headers);   
    if(!headers.access == "senha" || headers.access == undefined){
        res.json({error: "Access inválido"})
    }
    const body = new Pessoa(req.body);
    res.json({id: Number(`${body.id}`), nome: `${body.nome}`, idade: Number(`${body.idade}`)});
});